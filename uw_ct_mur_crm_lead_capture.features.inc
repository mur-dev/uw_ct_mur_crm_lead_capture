<?php

/**
 * @file
 * uw_ct_mur_crm_lead_capture.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_mur_crm_lead_capture_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_mur_crm_lead_capture_node_info() {
  $items = array(
    'uw_mur_crm_lead_capture' => array(
      'name' => t('MUR Lead Capture'),
      'base' => 'node_content',
      'description' => t('A customized lead capture form instance.'),
      'has_title' => '1',
      'title_label' => t('Sign Up Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_mur_crm_lead_capture_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: uw_mur_crm_lead_capture.
  $schemaorg['node']['uw_mur_crm_lead_capture'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  );

  return $schemaorg;
}

/**
 * Implements hook_modules_disabled().
 *
 * Disables this module if the uw_mur_crm_lead_capture module
 * is disabled.
 */
function uw_ct_mur_crm_lead_capture_modules_disabled($modules) {
  if (in_array('uw_mur_crm_lead_capture', $modules)) {
    $modules = ['uw_ct_mur_crm_lead_capture'];
    module_disable($modules, TRUE);
  }
}
