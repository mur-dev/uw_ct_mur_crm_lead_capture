<?php

/**
 * @file
 * uw_ct_mur_crm_lead_capture.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_mur_crm_lead_capture_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_crm_settings|node|uw_mur_crm_lead_capture|form';
  $field_group->group_name = 'group_crm_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_mur_crm_lead_capture';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'CRM Settings',
    'weight' => '7',
    'children' => array(
      0 => 'field_crm_assigned_id',
      1 => 'field_crm_campaign_id',
      2 => 'field_crm_targetlist_id',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-crm-settings field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_crm_settings|node|uw_mur_crm_lead_capture|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thank_you|node|uw_mur_crm_lead_capture|form';
  $field_group->group_name = 'group_thank_you';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_mur_crm_lead_capture';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thank You Settings',
    'weight' => '8',
    'children' => array(
      0 => 'field_thank_you_method',
      1 => 'field_thank_you_page_url',
      2 => 'field_thank_you_page_content',
      3 => 'field_thank_you_page_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-thank-you field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_thank_you|node|uw_mur_crm_lead_capture|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('CRM Settings');
  t('Thank You Settings');

  return $field_groups;
}
