<?php

/**
 * @file
 * uw_ct_mur_crm_lead_capture.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_ct_mur_crm_lead_capture_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_uw_mur_crm_lead_capture';
  $strongarm->value = '0';
  $export['comment_anonymous_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_uw_mur_crm_lead_capture';
  $strongarm->value = 0;
  $export['comment_default_mode_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_uw_mur_crm_lead_capture';
  $strongarm->value = '50';
  $export['comment_default_per_page_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_uw_mur_crm_lead_capture';
  $strongarm->value = 0;
  $export['comment_form_location_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_uw_mur_crm_lead_capture';
  $strongarm->value = '0';
  $export['comment_preview_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_uw_mur_crm_lead_capture';
  $strongarm->value = 0;
  $export['comment_subject_field_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_uw_mur_crm_lead_capture';
  $strongarm->value = '1';
  $export['comment_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_uw_mur_crm_lead_capture';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_uw_mur_crm_lead_capture';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_uw_mur_crm_lead_capture';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__uw_mur_crm_lead_capture';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'teaser' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'ical' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'entity_teaser' => array(
        'custom_settings' => FALSE,
      ),
      'embedded' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'locations' => array(
          'weight' => '12',
        ),
        'metatags' => array(
          'weight' => '13',
        ),
        'title' => array(
          'weight' => '-5',
        ),
        'path' => array(
          'weight' => '11',
        ),
        'redirect' => array(
          'weight' => '9',
        ),
        'xmlsitemap' => array(
          'weight' => '10',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_uw_mur_crm_lead_capture';
  $strongarm->value = '0';
  $export['language_content_type_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_uw_mur_crm_lead_capture';
  $strongarm->value = array();
  $export['menu_options_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_uw_mur_crm_lead_capture';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_mur_crm_lead_capture';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_uw_mur_crm_lead_capture';
  $strongarm->value = '1';
  $export['node_preview_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_uw_mur_crm_lead_capture';
  $strongarm->value = 0;
  $export['node_submitted_uw_mur_crm_lead_capture'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_uw_mur_crm_lead_capture';
  $strongarm->value = array(
    'status' => '0',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_node_uw_mur_crm_lead_capture'] = $strongarm;

  return $export;
}
