<?php

/**
 * @file
 * uw_ct_mur_crm_lead_capture.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_mur_crm_lead_capture_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_mur_crm_lead_capture content'.
  $permissions['create uw_mur_crm_lead_capture content'] = array(
    'name' => 'create uw_mur_crm_lead_capture content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_mur_crm_lead_capture content'.
  $permissions['delete any uw_mur_crm_lead_capture content'] = array(
    'name' => 'delete any uw_mur_crm_lead_capture content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_mur_crm_lead_capture content'.
  $permissions['delete own uw_mur_crm_lead_capture content'] = array(
    'name' => 'delete own uw_mur_crm_lead_capture content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_mur_crm_lead_capture content'.
  $permissions['edit any uw_mur_crm_lead_capture content'] = array(
    'name' => 'edit any uw_mur_crm_lead_capture content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_mur_crm_lead_capture content'.
  $permissions['edit own uw_mur_crm_lead_capture content'] = array(
    'name' => 'edit own uw_mur_crm_lead_capture content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_mur_crm_lead_capture revision log entry'.
  $permissions['enter uw_mur_crm_lead_capture revision log entry'] = array(
    'name' => 'enter uw_mur_crm_lead_capture revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_mur_crm_lead_capture authored by option'.
  $permissions['override uw_mur_crm_lead_capture authored by option'] = array(
    'name' => 'override uw_mur_crm_lead_capture authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_mur_crm_lead_capture authored on option'.
  $permissions['override uw_mur_crm_lead_capture authored on option'] = array(
    'name' => 'override uw_mur_crm_lead_capture authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_mur_crm_lead_capture comment setting'.
  $permissions['override uw_mur_crm_lead_capture comment setting option'] = array(
    'name' => 'override uw_mur_crm_lead_capture comment setting option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported: 'override uw_mur_crm_lead_capture promote to front page'.
  $permissions['override uw_mur_crm_lead_capture promote to front page option'] = array(
    'name' => 'override uw_mur_crm_lead_capture promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_mur_crm_lead_capture published option'.
  $permissions['override uw_mur_crm_lead_capture published option'] = array(
    'name' => 'override uw_mur_crm_lead_capture published option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_mur_crm_lead_capture revision option'.
  $permissions['override uw_mur_crm_lead_capture revision option'] = array(
    'name' => 'override uw_mur_crm_lead_capture revision option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_mur_crm_lead_capture sticky option'.
  $permissions['override uw_mur_crm_lead_capture sticky option'] = array(
    'name' => 'override uw_mur_crm_lead_capture sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_mur_crm_lead_capture content'.
  $permissions['search uw_mur_crm_lead_capture content'] = array(
    'name' => 'search uw_mur_crm_lead_capture content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
